javac -d bin \
../src/data/src/main/java/org/fins/data/constants/UserRoles.java \
../src/data/src/main/java/org/fins/data/dto/UserDto.java \
../src/data/src/main/java/org/fins/data/repository/UsersRepository.java \
../src/data/src/main/java/org/fins/data/DumbDumper.java

# java -cp bin org.fins.data.DumbDumper

javac -d bin -cp bin \
../src/filter/src/main/java/org/fins/filter/UsersFilter.java \
../src/filter/src/main/java/org/fins/filter/DumbFilterDumper.java

# java -cp bin org.fins.filter.DumbFilterDumper

javac -d bin -cp bin \
../src/printer/src/main/java/org/fins/printer/SimpleUsersPrinter.java \
../src/printer/src/main/java/org/fins/printer/DumbPrinterDumper.java

java -cp bin org.fins.printer.DumbPrinterDumper