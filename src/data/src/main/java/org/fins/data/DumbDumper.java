package org.fins.data;

import org.fins.data.dto.UserDto;
import org.fins.data.repository.UsersRepository;

public class DumbDumper {
    public static void main(String[] args) {
        UsersRepository repository = new UsersRepository();
        System.out.println("org.fins.data.DumbDumper");
        System.out.println("repository: " + repository);
        System.out.println("Users:");

        for (UserDto user : repository.getUsers()) {
            System.out.println("User.id " + user.getId());
            System.out.println("User.email" + user.getEmail());
            System.out.println("User.role" + user.getRole());
            System.out.println();
        }

        System.out.println("Good luck guys");
    }
}
