package org.fins.data.repository;

import org.fins.data.dto.UserDto;

import java.util.ArrayList;

import org.fins.data.constants.UserRoles;

public class UsersRepository {
    private ArrayList<UserDto> users = new ArrayList<>();

    public UsersRepository() {
        UserDto user1 = new UserDto();
        user1.setId(1);
        user1.setEmail("user1@mail.ru");
        user1.setRole(UserRoles.USER);

        UserDto user2 = new UserDto();
        user2.setId(2);
        user2.setEmail("user2@mail.ru");
        user2.setRole(UserRoles.USER);

        UserDto user3 = new UserDto();
        user3.setId(3);
        user3.setEmail("user3@mail.ru");
        user3.setRole(UserRoles.USER);

        UserDto admin1 = new UserDto();
        admin1.setId(4);
        admin1.setEmail("root@mail.ru");
        admin1.setRole(UserRoles.ADMIN);

        this.users.add(user1);
        this.users.add(user2);
        this.users.add(user3);
        this.users.add(admin1);

    }

    public ArrayList<UserDto> getUsers() {
        return this.users;
    }
}
