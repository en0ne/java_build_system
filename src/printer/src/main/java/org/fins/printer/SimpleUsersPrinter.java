package org.fins.printer;

import org.fins.data.dto.UserDto;

import java.util.ArrayList;
import java.util.List;

public class SimpleUsersPrinter {
    private List<UserDto> users;

    public SimpleUsersPrinter(ArrayList<UserDto> users) {
        this.users = users;
    }

    public void print() {
        for (UserDto user : this.users) {
            System.out.println("User row");
            System.out.println("User.id: " + user.getId());
            System.out.println("User.email: " + user.getEmail());
            System.out.println();
        }
    }
}
