package org.fins.printer;

import org.fins.data.constants.UserRoles;
import org.fins.data.repository.UsersRepository;
import org.fins.filter.UsersFilter;

public class DumbPrinterDumper {
    public static void main(String[] args) {
        UsersRepository repository = new UsersRepository();
        UsersFilter filter = new UsersFilter(repository);

        System.out.println("org.fins.printer.DumbPrinterDumper");
        System.out.println("Users with UserRole.ADMIN");
        SimpleUsersPrinter userPrinter = new SimpleUsersPrinter(filter.filterUsersByRole(UserRoles.ADMIN));
        userPrinter.print();

        System.out.println("Users with UserRole.USER");
        SimpleUsersPrinter adminPrinter = new SimpleUsersPrinter(filter.filterUsersByRole(UserRoles.USER));
        adminPrinter.print();

        System.out.println("Good luck guys");

    }
}
