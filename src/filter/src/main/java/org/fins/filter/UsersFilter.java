package org.fins.filter;

import org.fins.data.constants.UserRoles;
import org.fins.data.dto.UserDto;
import org.fins.data.repository.UsersRepository;

import java.util.ArrayList;

public class UsersFilter {
    private UsersRepository repository;

    public UsersFilter(UsersRepository repository) {
        this.repository = repository;
    }

    public ArrayList<UserDto> filterUsersByRole(UserRoles role) {
        ArrayList<UserDto> result = new ArrayList<>();

        for (UserDto user : repository.getUsers()) {
            if (user.getRole() == role) {
                result.add(user);
            }
        }

        return result;
    }
}
