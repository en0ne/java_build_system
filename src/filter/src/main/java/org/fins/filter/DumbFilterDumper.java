package org.fins.filter;

import org.fins.data.constants.UserRoles;
import org.fins.data.dto.UserDto;
import org.fins.data.repository.UsersRepository;

public class DumbFilterDumper {
    public static void main(String[] args) {
        UsersRepository repository = new UsersRepository();
        UsersFilter filter = new UsersFilter(repository);
        System.out.println("org.fins.filter.DumbFilterDumper");
        System.out.println("filter: " + filter);

        System.out.println("Users with UserRole.ADMIN");
        for (UserDto user : filter.filterUsersByRole(UserRoles.ADMIN)) {
            System.out.println("User.id: " + user.getId());
            System.out.println("User.email: " + user.getEmail());
            System.out.println("User.role: " + user.getRole());
            System.out.println();
        }

        System.out.println("Users with UserRole.USER");
        for (UserDto user : filter.filterUsersByRole(UserRoles.USER)) {
            System.out.println("User.id: " + user.getId());
            System.out.println("User.email: " + user.getEmail());
            System.out.println("User.role: " + user.getRole());
            System.out.println();
        }

        System.out.println("Good luck guys");
    }
}
